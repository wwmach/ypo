import * as mongoose from 'mongoose';

delete mongoose.connection.models['client'];

const Respondent = new mongoose.Schema({
	name: String,
	email: String,
	phone: String,
	guests: Number
})




export default mongoose.model('Respondent', Respondent);