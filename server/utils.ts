export class Status {
	public static deny = 401;
	public static error = 500;
	public static ok = 200;
}