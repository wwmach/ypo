import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import * as sanitize from 'mongo-sanitize';
import * as express from 'express';
import * as jwt from 'jsonwebtoken';

import Respondent from './models/respondent';

import { Status } from './utils';
import { Auth } from './middleware';

const router = express.Router();

mongoose.connect(ENV.mongoLocation);
const jsonParser = bodyParser.json();

router.post('/rsvp/signup', jsonParser, (req, res) => {
  let sanitizedBody = sanitize(req.body);
  let newRsvp = new Respondent(sanitizedBody);
  newRsvp.save((err, rsvp) => {
    if (err) res.sendStatus(Status.error);
    res.sendStatus(Status.ok);
  })
})

router.get('/rsvp/all', Auth, (req, res) => {
  Respondent.find({}, (err, users) => {
    if (err) res.sendStatus(Status.error);
    res.json(users);
  })
})

router.post('/login', jsonParser, (req, res) => {
  if (req.body.password === ENV.rsvpPassword) {
    jwt.sign({ data: 'woo'}, ENV.secret, { expiresIn: '1hr'}, (err, token) => {
      if (err) res.sendStatus(Status.error);
      else {
        res.set('authorization', token);
        res.sendStatus(Status.ok);
      }
    })
  } else {
    res.sendStatus(Status.deny)
  }
})

export default router;