import * as jwt from 'jsonwebtoken';
import { Status } from './utils';


export const Auth = (req, res, next) => {
	const auth = req.get('authorization');
	if (!auth) res.sendStatus(Status.deny);
	jwt.verify(auth, ENV.secret, (err, decoded) => {
		if (err) res.sendStatus(Status.deny);
		else next();
	})
}