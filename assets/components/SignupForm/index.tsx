import * as React from 'react';
import * as style from './SignupForm.scss';
import { Image, Select } from '../Pure';
import { connect } from 'react-redux';
import { Actions } from './Reducer';
import { Format } from '../../utils/Utils';

import { Spinner } from '../Pure';


class SignupForm extends React.Component<any, {}> {

  isValid() {
    const { valid } = this.props;
    return Object.keys(valid).reduce( (a, c) => a && valid[c], true);
  }

  renderSubmitted() {
    return (
      <div className={style.container}>
      <div className={style.content}>
        <Image className={style.logo} src='/res/logo.png' />
        <h1>ypo london getaway rsvp</h1>
        <h4>Thank you for taking the time to RSVP.</h4>
        <div className={style.form}>
          <h2>Thank you!</h2>
          <p>You will receive a confirmation email shortly</p>
        </div>
      </div>
    </div>
    )
  }

  render(): JSX.Element {
    const { dispatch, form, valid, submitPending, submitted } = this.props;
    const { email, guests, name, phone } = form;
    if (submitPending) {
      return <Spinner />
    }
    if (submitted) {
      return this.renderSubmitted();
    }
    return (
      <div className={style.container}>
        <div className={style.content}>
          <Image className={style.logo} src='/res/logo.png' />
          <h1>ypo london getaway rsvp</h1>
          <h4>Thank you for taking the time to RSVP.</h4>
          <div className={style.form}>
            <div> <label htmlFor='name'>Name</label>
              <input value={name} onChange={(e) => dispatch(Actions.updateName(e))} type='text' id='name' className={valid.name? '': style.invalid}/>
            </div>
            <div> <label htmlFor='email'>Email</label>
              <input value={email} onChange={(e) => dispatch(Actions.updateEmail(e))} type='email' id='email' className={valid.email? '': style.invalid} />
            </div>
            <div> <label htmlFor='phone'>Phone</label>
              <input value={Format.phoneNumber(phone)} onChange={(e) => dispatch(Actions.updatePhone(e))} type='text' id='phone' className={valid.phone? '': style.invalid} />
            </div>
            <div> <label htmlFor='guests'>Guests</label>
              <Select value={guests} className={style.select} list={[1,2,3,4,5,6,7,8,9,10]} onChange={(i, v) => dispatch(Actions.updateGuests(v))} />
            </div>
            <button disabled={!this.isValid()} onClick={() => dispatch(Actions.submitForm(form))} >Submit</button>
          </div>
        </div>
      </div>
    )
  }
}

export default connect( state => ({ ...state.RootReducer.SignupForm}))(SignupForm);