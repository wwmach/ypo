export class Actions {
  public static NAME_UPDATE = 'SIGNUPFORM_NAME_UPDATE';
  public static EMAIL_UPDATE = 'SIGNUPFORM_EMAIL_UPDATE';
  public static PHONE_UPDATE = 'SIGNUPFORM_PHONE_UPDATE';
  public static GUEST_UPDATE = 'SIGNUPFORM_GUEST_UPDATE';
  public static SUBMIT = {
    PENDING: 'SIGNUPFORM_SUBMIT_PENDING',
    SUCCESS: 'SIGNIPFORM_SUBMIT_SUCCESS',
    ERROR: 'SIGNUPFORM_SUBMIT_ERROR'
  }

  public static updateName({target}) {
    return {
      type: Actions.NAME_UPDATE,
      name: target.value
    }
  }
  public static updateEmail({target}) {
    return {
      type: Actions.EMAIL_UPDATE,
      email: target.value
    }
  }
  public static updatePhone({target}) {
    return {
      type: Actions.PHONE_UPDATE,
      phone: target.value
    }
  }
  public static updateGuests(guests) {
    return {
      type: Actions.GUEST_UPDATE,
      guests
    }
  }

  public static submitForm(form) {
    let t = Actions.SUBMIT;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/rsvp/signup',
      method: 'post',
      data: form
    }
  }
}

const initialState = {
  form: {
    name: '',
    email: '',
    phone: '',
    guests: 1
  },
  valid : {
    name: false,
    email: false,
    phone: false,
  },
  error: false,
  submitted: false,
  submitPending: false,
}

const isvalid = (str) => str && str.length > 0;
const isValidEmail = (str) => isvalid(str) && str.indexOf('@') > 0 && str.indexOf('.') > 0;

const updateValue = (state, key, value, valid) => ({
  ...state,
  form: {
    ...state.form,
    [key]: value[key]
  },
  valid: {
    ...state.valid,
    [key]: valid(value[key])
  }
})

export default (state = initialState, action) => {
  switch(action.type) {
    case Actions.NAME_UPDATE:
      return updateValue(state, 'name', action, isvalid);
    case Actions.EMAIL_UPDATE:
      return updateValue(state, 'email', action, isValidEmail);
    case Actions.PHONE_UPDATE:
      return updateValue(state, 'phone', action, (e) => e.length >= 14);
    case Actions.GUEST_UPDATE:
      return {...state, form: { ...state.form, guests: action.guests }}
    case Actions.SUBMIT.PENDING:
      return {...state, submitPending: true, error: false }
    case Actions.SUBMIT.SUCCESS:
      return {...state, submitPending: false, submitted: true}
    case Actions.SUBMIT.ERROR:
      return {...state, submitPending: false, error: true}
    // case Actions.SHOW_ERRORS:
    //   return {...state, showErrors: true}
  }
  return state;
}