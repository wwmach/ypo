import * as React from 'react';
import * as style from './Grid.scss';
import { connect } from 'react-redux';
import { Actions } from './Reducer';

import { Spinner } from '../Pure';

import { Format } from '../../utils/Utils';


class Grid extends React.Component<any, {}> {

  componentDidMount() {
    this.props.dispatch(Actions.fetch());
  }
  render(): JSX.Element {
    const { dataset, dispatch, loading, sortKey, sortDir } = this.props;
    if (loading) {
      return <Spinner />
    }
    let cols: any = []
    if (dataset.length > 0) {
      cols = Object.keys(dataset[0]).filter(i => i!=='_id' && i!=='__v');
    }

    const headerMap = (k, i) => <th key={i} className={sortKey===k? sortDir? style.asc : style.desc : ''} onClick={() => dispatch(Actions.sortBy(k))}>{Format.capitalize(k)}</th>
    const itemMap = (k, i) => <td key={i}>{k}</td>


    return (
      <div className={style.container}>
        <div className={style.innerContainer}>
          <h2>RSVP Respondents</h2>
          <table>
            <thead>
              <tr>{cols.map(headerMap)}</tr>
            </thead>
            <tbody>
              {dataset.map( (d, i) => <tr key={i}>{cols.map((k, i) => itemMap(d[k],i))}</tr>)}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default connect( state => ({...state.RootReducer.AdminGrid}))(Grid);