export class Actions {
  public static FETCH = {
    PENDING: 'GRID_FETCH_PENDING',
    SUCCESS: 'GRID_FETCH_SUCCESS',
    ERROR: 'GRID_FETCH_ERROR'
  }

  public static SORT_BY = 'GRID_SORT_BY';

  public static fetch() {
    let t = Actions.FETCH;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/rsvp/all'
    }
  }

  public static sortBy(key) {
    return { type: Actions.SORT_BY, key}
  }
}

const initialState = {
  loading: true,
  error: false,
  dataset: [],
  sortKey: 'name',
  sortDir: true // true: asc, false: desc
}

const sortBy = ({sortKey, sortDir}) =>
  (a, b) => (sortDir? a[sortKey]<b[sortKey] : b[sortKey]<a[sortKey])? 1: -1;

export default (state=initialState, action) => {
  switch (action.type) {
    case Actions.FETCH.PENDING:
      return {
        ...state,
        loading: true,
        error: false
      }
    case Actions.FETCH.SUCCESS:
      return {
        ...state,
        loading: false,
        dataset: action.data.sort(sortBy(state))
      }
    case Actions.FETCH.ERROR:
      return {
        ...state,
        loading: false,
        error: true
      }
    case Actions.SORT_BY:
      let newState = {
        ...state,
        sortKey: action.key,
        sortDir: state.sortKey===action.key? !state.sortDir : state.sortDir
      }
      newState.dataset = state.dataset.sort(sortBy(newState));
      return  newState;
  }
  return state;
}