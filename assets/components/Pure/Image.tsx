import * as React from 'react';

import * as style from './Image.scss';

export default class Image extends React.Component<any, {}> {
	render(): JSX.Element {
		const { src, className='' } = this.props;
		return <div {...this.props} className={`${style.container} ${className}`} style={{backgroundImage: `url("${src}")`}} />
	}
}