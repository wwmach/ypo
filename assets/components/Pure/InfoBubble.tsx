import * as React from 'react';
import PopoutMenu from './PopoutMenu';

import * as style from './InfoBubble.scss';

interface Props {
	msg?: string;
	hoverMsg: { title:string,text:string|JSX.Element}|string;
	className?: string;
}

export default class InfoBubble extends React.Component<Props, {hovered: boolean}> {
	constructor(props) {
		super(props);
		this.state = { hovered: false};
	}

	render(): JSX.Element {
		const { msg, hoverMsg } = this.props;
		let hoverTitle = typeof hoverMsg === 'object'? hoverMsg.title : '';
		let hoverText = typeof hoverMsg === 'object'? hoverMsg.text : hoverMsg;
		let spread = {...this.props};
		delete spread.hoverMsg
		delete spread.msg;

		return (
			<div className={style.container}
				onMouseEnter={() => this.setState({hovered: true})}
				onMouseLeave={() => this.setState({hovered: false})}
				data-msg={msg}>
					<PopoutMenu open={this.state.hovered || false}>
						<div {...spread}>
							<b>{hoverTitle}</b>
							{typeof hoverText === 'string'? <p>{hoverText}</p> : hoverText}
						</div>
					</PopoutMenu>
			</div>
		)
	}
}