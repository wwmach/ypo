import * as React from 'react';

import * as style from './PopoutMenu.scss';

interface Props {
	open: boolean;
	className?: string;
  onClose?: () => void;
  direction?: 'left'|'right';
}

export default class PopoutMenu extends React.Component<Props, {}> {
	render(): JSX.Element|null {
		const { onClose, direction='right' } = this.props;
		if (!this.props.open) {
			return null;
		}
		return (
			<div className={style.container}>
				<div className={direction ==='right'? style.innerContainerRight : style.innerContainerLeft}>
					{ onClose? <div className={style.close} onClick={onClose} /> : null }
					<div className={this.props.className}>
					{this.props.children}
					</div>
				</div>
			</div>
		)
	}
}