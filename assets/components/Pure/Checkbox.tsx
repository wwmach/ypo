import * as React from 'react';
import * as style from './Checkbox.scss';

interface Props {
  checked: boolean;
  error?: boolean;
  onClick: (checked: boolean) => void;
}

export default class Checkbox extends React.Component<Props, {}> {
  static defaultProps = {
    error: false,
    checked: false,
  }

  render(): JSX.Element {
    let { error, checked, onClick } = this.props;
    let className = error? style.error : (checked? style.checked : style.unchecked)
    return (
      <div className={style.container} onClick={() => onClick(!checked)}>
        <div className={className} />
        {this.props.children}
      </div>
    )
  }

}