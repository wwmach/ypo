import * as React from 'react';
import * as style from './Modal.scss';

import Image from './Image';

interface Button {
  text: string;
  primary?: boolean;
}

interface Props {
  className?: string;
  icon?: string;
  title?: string;
  text?: string;
  buttons?: Button[];
  open: boolean;
  onClick?: any;
  onClose?: () => void;
}

export default class Modal extends React.Component<Props, any> {
  static defaultProps = {
    className: '',
    onClick: (button) => console.error(`onClick('${button}': string) not implemented`),
  }

  onClick(item: string) {
    this.props.onClick(item);
  }

  renderButton(b: Button, index: number): JSX.Element {
    const { text, primary=false} = b;
    return (
      <button key={index} className={primary? style.buttonPrimary : style.button} onClick={() => this.onClick(text)}>
        {text}
      </button>
    )
  }

  render(): JSX.Element|null {
    const { open, className,
      icon, title,
      text, buttons=[],
      children, onClose } = this.props;
    if (!open) {
      return null;
    }

    return (
      <div className={style.shade}>
        <div className={`${className} ${style.container}`}>
          {onClose? <div className={style.close} onClick={onClose} />: null}
          {icon? <Image src={icon} className={style.image} /> : null }
          {title? <div className={style.title}>{title}</div> : null }
          {text? <div className={style.text}>{text}</div> : null }
          { children }
          {buttons? <div className={style.buttonContainer}>
            {buttons.map((b, i) => this.renderButton(b,i))}
          </div>: null}
        </div>
      </div>
    )
  }
}