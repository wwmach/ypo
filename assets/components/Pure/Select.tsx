import * as React from 'react';

import * as style from './Select.scss';

interface Props {
	label?: string;
	list: (string|number)[];
	onChange: (number, string) => void;
  value?: string|number;
  className?: string;
}

interface State {
	selected: number;
	open: boolean;
}

export default class Select extends React.Component<Props, State> {
	constructor(props) {
    super(props);
		this.state = {
			selected: props.value? props.list.indexOf(props.value) : -1,
			open: false
		}
	}

	static getDefaultProps = {
		label: 'Select'
	}

	toggle(): void {
		this.setState({ open: !this.state.open });
	}

	select(i: number): void {
		this.setState({ selected: i});
		this.props.onChange(i, this.props.list[i]);
	}

	renderItem(item: string, i: number): JSX.Element {
		let { selected } = this.state;
		return <li key={i} onClick={() => this.select(i>=selected && selected >=0? i+1 : i)}>{item}</li>
	}

	render(): JSX.Element {
		const {selected, open } = this.state;
		const { label } = this.props;
		const list = this.props.list.filter((s, i) => i !== selected);
		const showLabel = selected < 0 || typeof this.props.list[selected] === 'undefined';
		return (
			<div className={`${style.container} ${this.props.className? this.props.className : ''}`}>
				<ul className={`${style.select} ${open? style.open : ''}`} onClick={() => this.toggle()}>
					<li className={`${showLabel? style.label : ''}`} key={selected}>
						{showLabel? label : this.props.list[selected]}
					</li>
					{ open? list.map(this.renderItem.bind(this)) : null }
				</ul>
			</div>
		)
	}
}