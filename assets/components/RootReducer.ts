import { combineReducers } from 'redux';
import SignupForm from './SignupForm/Reducer';
import Login from './Login/Reducer';
import AdminGrid from './Grid/Reducer';


const initialWindowState = {
  width: ENV.BUILD_TARGET === 'client'? window.innerWidth : 0,
  height: ENV.BUILD_TARGET === 'client'? window.innerHeight: 0,
  tabletBreakpoint: 1299,
  phoneBreakpoint: 896
}

const SCREEN_RESIZE = 'WINDOW_SCREEN_RESIZE';

export function attachResizeListener(store) {
  if (ENV.BUILD_TARGET === 'client') {
    let timer;
    window.addEventListener('resize', e => {
      // assuming a large number of components will be using screen size
      // only dispatch the action after the resize has finished
      clearTimeout(timer);
      timer = setTimeout(() => {
        store.dispatch(screenResize(window.innerWidth, window.innerHeight))
      }, 250);
    })
  }
}

function screenResize(width, height) {
  return {
    type: SCREEN_RESIZE,
    width, height
  }
}

function bounds(state = initialWindowState, action) {
  switch (action.type) {
    case SCREEN_RESIZE:
      return {
        ...state,
        width: action.width,
        height: action.height
      }
    case '@@router/LOCATION_CHANGE':
      return { ...state, width: window.innerWidth, height: window.innerHeight }
  }
  return state;
}

function pathHistory(state = [], action) {
  if (action.type === 'PATH_TRACKING') {
    let lastPath = state[0];
    return lastPath === action.path ? state : [ action.path, ...state];
  }
  return state;
}

const reducer = combineReducers({
  pathHistory,
  bounds,
  SignupForm,
  Login,
  AdminGrid
})

export default reducer;
