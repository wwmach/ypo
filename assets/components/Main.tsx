import * as React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import Signup from '../pages/Signup';
import AdminView from '../pages/AdminView';
import Login from '../pages/Login';
import { P404 } from '../pages/404';
import { P403 } from '../pages/403';

import { requireAuth } from '../utils/Utils';

export default class Main extends React.Component<{}, {}> {
  render(): JSX.Element {
    return (
      <div className='mainContent'>
        <Switch>
          <Route exact path='/' component={Signup}/>
          <Route path='/rsvp' component={AdminView} />
          <Route path='/login' component={Login}/>

          {/* <Route path='*' component={P403}/> */}
          <Route path='*' component={P404}/>
        </Switch>
      </div>
    )
  }
}
