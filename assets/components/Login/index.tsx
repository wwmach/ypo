import * as React from 'react';
import * as style from './Login.scss';
import { connect } from 'react-redux';

import { Actions } from './Reducer';
// import { Loading } from '../Pure';

import { Spinner } from '../Pure';

class Login extends React.Component<any, {}> {

  submit() {
    let input = (document.getElementById('password') as HTMLInputElement);
    if (input) {
      this.props.dispatch(Actions.login({ password: input.value}));
    }
  }

  render(): JSX.Element {
    const { error, loading, authorized } = this.props;
    if (loading) {
      return <Spinner />
    }
    return (
      <div className={style.container}>
        <div className={style.content}>
          {/* <Image className={style.logo} src='/res/logo.png' /> */}
          <h1>Viewer of RSVPs</h1>
          <h4>What is the password?</h4>
          <div className={style.form}>
            {error? <div className={style.error}>Wrong password, try again</div> : null }
            <div> <label htmlFor='password'>Password</label>
              <input type='password' id='password'/>
            </div>
            <button onClick={() => this.submit()} >Submit</button>
          </div>
        </div>
      </div>
    )
  }
}

export default connect( state => ({...state.RootReducer.Login}))(Login);