export class Actions {
  public static LOGIN = {
    PENDING: 'LOGIN_LOGIN_PENDING',
    SUCCESS: 'LOGIN_LOGIN_SUCCESS',
    ERROR: 'LOGIN_LOGIN_ERROR'
  }

  public static login(data) {
    let t = Actions.LOGIN;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/login',
      method: 'post',
      data
    }
  }
}

const initialState = {
  loading: false,
  authorized: false,
  error: false,
}

export default (state=initialState, action) => {
  switch(action.type) {
    case Actions.LOGIN.PENDING:
      return {
        ...state,
        loading: true,
        error: false
      }
    case Actions.LOGIN.SUCCESS:
      return {
        ...state,
        loading: false,
        authorized: true,
      }
    case Actions.LOGIN.ERROR:
      return {
        ...state,
        loading: false,
        error: true
      }
  }
  return state;
}