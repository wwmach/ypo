import * as pdfMake from 'pdfmake/build/pdfmake';
import * as vfsFonts from 'pdfmake/build/vfs_fonts';

declare var Promise;

export class Format {
	static price(price: number, {type = 'USD', region = 'United States'}): string {
		// let precision = (price === (price | 0))? 0 : 2
		if (type.length ===0) type = 'USD';
		if (region.length === 0) region = 'United States';
		let locale = getCurrencySymbol(type);
    let precision = 2;

		return `${locale}${price.toLocaleString(type, {
			minimumFractionDigits: precision,
			maximumFractionDigits: precision
		})}${showCurrencyType({type, region})? ' '+type : ''}`;
	}
	static percentage(percentage: number): string {
		return `${(percentage * 100).toLocaleString()}%`;
	}
	static dateFull(timestamp: number): string {
		return new Date(timestamp)
			.toLocaleDateString(undefined, {
				month: 'long',
				day: 'numeric',
				year: 'numeric'
			})
	}

	static date(timestamp: number): string {
		let date = new Date(timestamp);
		return `${Format.padLeft((date.getMonth()+1).toString(),2,'0')}/${Format.padLeft(date.getDate().toString(), 2, '0')}/${date.getFullYear().toString().slice(2)}`
	}

	static padLeft(str: string, x: number, s: string): string {
		str = str.toString();
		if (str.length >= x) return str;
		return Array((x+1-str.length)/s.length | 0).join(s)+str;
	}

  static capitalize(str:string): string {
    if (str.length<1) return str;
    return str[0].toUpperCase()+str.slice(1);
	}

	static pluralize(str:string, qty: number): string {
		return qty>1 && str[str.length-1] !== 's' ? str+'s' : str;
  }
  static depluralize(str: string): string {
    return str[str.length-1] === 's'? str.substr(0, str.length-1) : str;
  }

  static phoneNumber(str:string): string {
		let s = str.replace(/\D/g,'');
		let newStr: any = '';
		if (s.length > 0) {
			newStr = `(${s.slice(0,3)}`
		}
		if (s.length > 3) {
			newStr += `) ${s.slice(3,6)}`;
		}
		if (s.length > 6) {
			newStr +=`-${s.slice(6)}`
		}
    return newStr;//`(${s.slice(0,3)}) ${s.slice(3,6)}-${s.slice(6)}`
	}

	static stringifyList(strs: string[]): string {
		return strs.slice(0,-1).join(', ')+', and '+strs.slice(-1);
	}
}

export function NewPdfMake() {
	const {vfs} = vfsFonts.pdfMake;
	pdfMake.vfs = vfs;
	return pdfMake;
}

export function lockScroll(lock:boolean = true, resetScroll: boolean = false) {
  // const [ xOffset, yOffset ] = [ window.pageXOffset, window.pageYOffset];
  if (resetScroll) {
    window.scroll(0, 0);
  }
  document.body.classList.toggle('body-locked', lock);
  // return () => {
  //   document.body.classList.remove('.body-locked');
  //   window.scroll(xOffset, yOffset);
  // }
}

export function getProportionateWidth({width, height}, targetHeight) {
	return width * targetHeight / height;
}

export function ImageBase64(url: string) {
  return new Promise((resolve, reject) => {
    let image = new Image();
    image.setAttribute('crossOrigin', 'anonymous');
    image.onerror = (e) => reject(e);
    image.onload = function() {
      let me = this as HTMLImageElement;
      let canvas = document.createElement('canvas');
      canvas.width = me.width;
      canvas.height = me.height;
      let ctx = canvas.getContext('2d');
      if (ctx !== null) {
        // ctx.scale(2,2);
        ctx.drawImage(me, 0, 0);
				let dataUrl = canvas.toDataURL("image/png");
				if (dataUrl) {
					resolve({data: dataUrl, width: canvas.width, height: canvas.height});
				} else { reject('failed to get dataurl')}
      } else {
        reject('failed to create canvas');
      }
    }
    image.src = url;
  });
}

export function serialize(params) {
  return Object.keys(params).map( p => `${p}=${encodeURIComponent(params[p].trim())}`).join('&');
}

export function composeEmail(recipients: string[], subject: string, body: string) {
  return `mailto:${encodeURIComponent(recipients.join(', '))}?${serialize({subject, body})}`;
}

export function mapObjectWithIndex(obj: any, currier: any, index: number) {
  let keys = Object.keys(obj);
  return keys.map(k => ({key: k, func: (data) => currier(obj[k](index,data))}))
    .reduce((a, c) => ({...a, [c.key]: c.func}), {});
}

// http://www.thefinancials.com/Default.aspx?SubSectionID=curformat
export function getCurrencySymbol(abrev: string): string {
	switch (abrev.toUpperCase()) {
		case 'EUR':
			return '€';
		case 'USD':
		case 'CAD':
		case 'AUD':
		default:
			return '$';
	}
}

export function showCurrencyType(currency: Currency): boolean {
	return !(currency.type === 'USD' && currency.region === 'United States');
}

export function requireAuth(nextState, replace) {
  replace({
    pathname: '/login'
  });
}

export function AuthRoute({ component: Component, ...rest}) {
  // return (

  // )
}