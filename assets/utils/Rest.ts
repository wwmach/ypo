import 'isomorphic-fetch';

import { Actions } from '../components/Login/Reducer';
import { push } from 'react-router-redux';

export default store => next => action => {
  if (typeof action.type ==='undefined' || typeof action.url === 'undefined') return next(action);

  let state = store.getState();

  let [ pendingType, successType, errorType ] = action.type;

  let {
    url,
    method = 'get',
    contentType = 'application/json',
    query = {},
    data = {},
  } = action;

  let req = {
    headers: {'Content-Type': contentType},
    method,
  }

  let token = window.localStorage.getItem(ENV.authToken);

  if (method.toLowerCase() === 'post') {
    req['body'] = JSON.stringify(data);
  }

  if (url.indexOf('://') < 0) {
    url = ENV.apiRoot + url;
    if (token) {
      req.headers['Authorization'] = token;
    }
  }

  next({ type: pendingType })

  if (ENV.DEPLOY_TARGET === ENV.TARGET_DEV) {
    console.log("sending fetch", url, req);
  }

  fetch(url, req)
  .then(r => {
    if (r.status === 401) {
        window.localStorage.removeItem(ENV.authToken);
        next(push('/login'));
        // TODO redirect to login page here
    }
    if (!r.ok) {
      throw r;
    }
    if (r.ok && successType === Actions.LOGIN.SUCCESS && r.headers.has("authorization")) {
      window.localStorage.setItem(ENV.authToken, r.headers.get("authorization") || '');
      window.history.back();
      return;
    }
    let type = r.headers.get('content-type');
    return type && type.indexOf('application/json')>=0? r.json() : 'ok';
  })
  .then(data => {
    next({
      type: successType,
      data,
      meta: action.meta
    })
  }).catch(e => {
    next({
      type: errorType,
      error: e
    })
    return;
  })
}