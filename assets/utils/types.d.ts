declare interface Currency {
  type: string;
  region: string;
}