import * as React from 'react';

import SignupForm from '../components/SignupForm';

export default class Signup extends React.Component<{}, {}> {
  render(): JSX.Element {
    return <SignupForm />
  }
}